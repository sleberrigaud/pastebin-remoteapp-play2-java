import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

    val appName         = "paste-bin-play_2-java"
    val appVersion      = "1.0-SNAPSHOT"

    val appDependencies = Seq(
      javaCore,
      javaEbean,
      "com.atlassian.plugins" % "play-java-ap3_2.10" % "0.4.3",
      "org.leberrigaud.pastebin" % "pastebin-utils" % "0.2",
      "postgresql" % "postgresql" % "8.4-701.jdbc3",
      "commons-lang" % "commons-lang" % "2.6"
    )

    val main = play.Project(appName, appVersion, appDependencies).settings(
        resolvers += "Atlassian's Maven Public Repository" at "https://maven.atlassian.com/content/groups/public",
        resolvers += "LeBerrigaud.Org's Repository" at "http://leberrigaud-repository.googlecode.com/svn/releases",
        resolvers += "Local Maven Repository" at "file://" + Path.userHome + "/.m2/repository"
    )
}
