# --- !Ups

create table paste (
  id                        bigint not null,
  hash                      varchar(255) not null,
  key                       varchar(255) not null,
  constraint uq_paste_hash unique (hash),
  constraint pk_paste primary key (id))
;

create sequence paste_seq;

# --- !Downs

drop table if exists paste cascade;

drop sequence if exists paste_seq;
