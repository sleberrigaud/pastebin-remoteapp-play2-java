package models;

import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public final class Paste extends Model
{
    @Id
    public Long id;

    // the hash of the code string
    @Column(unique = true, nullable = false)
    public String hash;

    // the key to the paste at pastebin
    @Column(nullable = false)
    public String key;

    public static void create(Paste p)
    {
        p.save();
    }

    public static Paste findByHash(String hash)
    {
        return find.where().eq("hash", hash).findUnique();
    }

    public static void delete(Long id)
    {
        find.ref(id).delete();
    }

    private static Finder<Long, Paste> find = new Finder<Long, Paste>(Long.class, Paste.class);
}
