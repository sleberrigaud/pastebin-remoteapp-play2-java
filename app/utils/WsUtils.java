package utils;

import com.google.common.base.Joiner;
import play.libs.F;
import play.libs.WS;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class WsUtils
{
    private static final int DEFAULT_TIMEOUT = secondsToMilliseconds(2);

    public static F.Promise<WS.Response> post(String url, Map<String, String> parameters)
    {
        return postWithTimeOutInMillis(url, parameters, DEFAULT_TIMEOUT);
    }

    public static F.Promise<WS.Response> post(String url, Map<String, String> parameters, int timeout)
    {
        return postWithTimeOutInMillis(url, parameters, secondsToMilliseconds(timeout));
    }

    private static F.Promise<WS.Response> postWithTimeOutInMillis(String url, Map<String, String> parameters, int timeoutInMillis)
    {
        return WS.url(url)
                .setTimeout(timeoutInMillis)
                .setHeader("Content-Type", "application/x-www-form-urlencoded")
                .post(urlEncodedParams(parameters));
    }

    static String urlEncodedParams(Map<String, String> params)
    {
        return Joiner.on('&').join(params.entrySet());
    }

    private static int secondsToMilliseconds(int seconds)
    {
        return Long.valueOf(TimeUnit.MILLISECONDS.convert(seconds, TimeUnit.SECONDS)).intValue();
    }
}
