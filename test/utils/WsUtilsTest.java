package utils;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public final class WsUtilsTest
{
    @Test
    public void testUrlEncodedParams() throws Exception
    {
        assertEquals("param1=value1&param2=value2", WsUtils.urlEncodedParams(ImmutableMap.of("param1", "value1", "param2", "value2")));
    }
}
